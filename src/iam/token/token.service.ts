import { CACHE_MANAGER, Cache } from '@nestjs/cache-manager';
import { Inject, Injectable } from '@nestjs/common';
import { randomBytes } from 'crypto';
import { TOKEN_REDIS_KEY } from './token.const';
import { TokenType } from './token.types';

@Injectable()
export class TokenService {
  constructor(@Inject(CACHE_MANAGER) private readonly cacheManager: Cache) {}

  private getKey(id: string, type: TokenType) {
    return `${id}-${type}-${TOKEN_REDIS_KEY}`;
  }

  async issueToken(id: string, type: TokenType) {
    const token = randomBytes(32).toString('hex');

    await this.cacheManager.set(this.getKey(id, type), token);

    return token;
  }

  async removeToken(id: string, type: TokenType) {
    await this.cacheManager.del(this.getKey(id, type));
  }

  async validateToken(id: string, type: TokenType, token: string) {
    const userToken = await this.cacheManager.get(this.getKey(id, type));

    return userToken === token;
  }
}
