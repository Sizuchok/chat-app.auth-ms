import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

@Injectable()
export class BcryptService {
  async hash(data: string) {
    return bcrypt.hash(data, 10);
  }

  async compare(plainData: string, hashedData: string) {
    return bcrypt.compare(plainData, hashedData);
  }
}
