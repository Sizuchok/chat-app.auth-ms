import {
  BadRequestException,
  ForbiddenException,
  Inject,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ClientKafka } from '@nestjs/microservices';
import * as queryString from 'querystring';
import { NOTIFICATIONS_MS_KEY } from '../../common/const/ms-keys.const';
import { USER } from '../../common/const/user-events.const';
import { UserWithoutPasswordDto } from '../../users/dto/user-without-password.dto';
import { UsersService } from '../../users/users.service';
import { BcryptService } from '../bcrypt/bcrypt.service';
import { TokenService } from '../token/token.service';
import { SignInRequestDto } from './dto/sign-in-request.dto';
import { SignUpRequestDto } from './dto/sign-up-request.dto';
import { VerifyAccountRequestDto } from './dto/verify-account-request.dto';

@Injectable()
export class AuthenticationService {
  constructor(
    @Inject(NOTIFICATIONS_MS_KEY) private notificationsClient: ClientKafka,
    private readonly bcryptService: BcryptService,
    private readonly jwtService: JwtService,
    private readonly tokenService: TokenService,
    private readonly usersService: UsersService,
  ) {}

  async signUp(signUpRequestDto: SignUpRequestDto) {
    const user = await this.usersService.createUser(signUpRequestDto);

    const token = await this.tokenService.issueToken(user.id, 'VERIFICATION');
    const verificationLink = this.generateVerificationLink(user.id, token);

    // What if an error while sending an email will occur?
    this.notificationsClient.emit(USER.NEW, {
      name: user.name,
      email: user.email,
      verificationLink,
    });
  }

  private generateVerificationLink(id: string, token: string) {
    const queryParams = queryString.stringify({ token, id });
    return `${process.env.FRONT_END_URL}/auth/verify?${queryParams}`;
  }

  async signIn({ password, uniqueId }: SignInRequestDto) {
    const errorMessage = 'Invalid email (nickname) or password' as const;

    const newUser = await this.usersService.findUserByUniqueId(uniqueId);

    if (!newUser) {
      throw new UnauthorizedException(errorMessage);
    }

    const isEqual = await this.bcryptService.compare(password, newUser.password);

    if (!isEqual) {
      throw new UnauthorizedException(errorMessage);
    }

    if (!newUser.isVerified) {
      throw new ForbiddenException(
        'Account not verified. Please verify your account to proceed with login.',
      );
    }

    const accessToken = await this.jwtService.signAsync({ sub: newUser.id, email: newUser.email });

    const user = new UserWithoutPasswordDto(newUser);

    return { accessToken, user };
  }

  async verifyAccount({ id, token }: VerifyAccountRequestDto) {
    const isValid = await this.tokenService.validateToken(id, 'VERIFICATION', token);

    if (!isValid) {
      throw new UnauthorizedException('Invalid or expired verification link');
    }

    await this.tokenService.removeToken(id, 'VERIFICATION');

    const user = await this.usersService.findUserByIdDontThrow(id);

    if (!user) {
      throw new BadRequestException('User not found');
    }

    user.isVerified = true;
    await user.save();
  }
}
