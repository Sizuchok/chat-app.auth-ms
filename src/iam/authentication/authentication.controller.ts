import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { AuthenticationService } from './authentication.service';
import { SignInRequestDto } from './dto/sign-in-request.dto';
import { SignUpRequestDto } from './dto/sign-up-request.dto';
import { VerifyAccountRequestDto } from './dto/verify-account-request.dto';
import { Public } from '../decorators/public/public.decorator';

@Public()
@Controller('auth')
export class AuthenticationController {
  constructor(private readonly authService: AuthenticationService) {}

  @Post('sign-up')
  async signUp(@Body() signUpDto: SignUpRequestDto) {
    return this.authService.signUp(signUpDto);
  }

  @Post('sign-in')
  @HttpCode(HttpStatus.OK)
  async signIn(@Body() signInDto: SignInRequestDto) {
    return this.authService.signIn(signInDto);
  }

  @Post('verify')
  @HttpCode(HttpStatus.NO_CONTENT)
  async verifyAccount(@Body() verifyAccountRequestDto: VerifyAccountRequestDto) {
    await this.authService.verifyAccount(verifyAccountRequestDto);
  }
}
