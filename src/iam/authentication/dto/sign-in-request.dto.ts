import { Type } from 'class-transformer';
import { IsNotEmpty, IsString, IsStrongPassword } from 'class-validator';

export class SignInRequestDto {
  @IsNotEmpty()
  @IsString()
  uniqueId: string;

  @Type()
  @IsStrongPassword()
  password: string;
}
