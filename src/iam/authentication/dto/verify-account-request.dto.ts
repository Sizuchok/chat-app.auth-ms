import { IsMongoId, IsNotEmpty, IsString } from 'class-validator';

export class VerifyAccountRequestDto {
  @IsMongoId()
  id: string;

  @IsNotEmpty()
  @IsString()
  token: string;
}
