import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from '../users/users.module';
import { AuthenticationController } from './authentication/authentication.controller';
import { AuthenticationService } from './authentication/authentication.service';
import { BcryptModule } from './bcrypt/bcrypt.module';
import { accessTokenConfig } from './configs/acess-token.config';
import { AuthGuard } from './guards/auth.guard';
import { TokenService } from './token/token.service';

@Module({
  imports: [JwtModule.registerAsync(accessTokenConfig.asProvider()), UsersModule, BcryptModule],
  controllers: [AuthenticationController],
  providers: [AuthenticationService, TokenService, { provide: APP_GUARD, useClass: AuthGuard }],
  exports: [],
})
export class IamModule {}
