import { registerAs } from '@nestjs/config';
import { JwtModuleAsyncOptions } from '@nestjs/jwt';

export const accessTokenConfig = registerAs('accessToken', () => {
  return {
    secret: process.env.JWT_SECRET_KEY,
    signOptions: { expiresIn: `${process.env.JWT_TTL}s` },
  } as JwtModuleAsyncOptions;
});
