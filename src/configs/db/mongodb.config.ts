import { registerAs } from '@nestjs/config';
import { MongooseModuleFactoryOptions } from '@nestjs/mongoose';

export const mongodbConfig = registerAs('mongodb', () => {
  return {
    uri: process.env.MONGO_DB_URL,
    dbName: process.env.MONGO_DB_NAME,
  } as MongooseModuleFactoryOptions;
});
