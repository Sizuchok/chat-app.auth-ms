import { IsOptional, IsPositive, IsInt, Min } from 'class-validator';

export class BasePaginationQueryDto {
  @IsOptional()
  @IsPositive()
  @IsInt()
  limit?: number;

  @IsOptional()
  @IsInt()
  @Min(0)
  skip?: number;
}
