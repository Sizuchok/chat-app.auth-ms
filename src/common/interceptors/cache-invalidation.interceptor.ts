import { Cache } from '@nestjs/cache-manager';
import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CACHE_INVALIDATE_KEY } from '../decorators/invalidate-cache.const';
import { RedisStore } from 'cache-manager-ioredis-yet';
import { getPaginatedRedisKey } from '../util/get-paginated-redis-key.util';

@Injectable()
export class CacheInvalidationInterceptor implements NestInterceptor {
  constructor(private readonly cacheManager: Cache, private readonly reflector: Reflector) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<unknown> {
    const cacheKey = this.reflector.get<string>(CACHE_INVALIDATE_KEY, context.getHandler());

    // TODO: what if an error occurs in a controller
    return next.handle().pipe(
      tap(async () => {
        if (cacheKey) {
          const redis = this.cacheManager.store as RedisStore;
          const keys = await redis.client.keys(
            getPaginatedRedisKey(cacheKey, { limit: '*', skip: '*' }),
          );
          const delPromises = keys.map((key) => this.cacheManager.del(key));

          await Promise.all(delPromises);
        }
      }),
    );
  }
}
