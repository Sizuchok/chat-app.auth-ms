import { CACHE_KEY_METADATA, Cache, CacheInterceptor } from '@nestjs/cache-manager';
import { ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import { getPaginatedRedisKey } from '../util/get-paginated-redis-key.util';

@Injectable()
export class PaginatedCacheInterceptor extends CacheInterceptor {
  constructor(protected readonly cacheManager: Cache, protected readonly reflector: Reflector) {
    super(cacheManager, reflector);
  }
  protected trackBy(context: ExecutionContext): string | undefined {
    const query = context.switchToHttp().getRequest<Request>().query;
    const cacheKey = this.reflector.get<string>(CACHE_KEY_METADATA, context.getHandler());

    const key = getPaginatedRedisKey(cacheKey, query);
    return key;
  }
}
