export const AUTH_MS_CLIENT_ID = 'auth-ms' as const;
export const AUTH_MS_CONSUMER_ID = 'auth-ms-consumer' as const;

export const NOTIFICATIONS_MS_CLIENT_ID = 'notifications-ms' as const;
export const NOTIFICATIONS_MS_CONSUMER_ID = 'notifications-ms-consumer' as const;

export const CHAT_MS_CLIENT_ID = 'chat-ms' as const;
export const CHAT_MS_CONSUMER_ID = 'chat-ms-consumer' as const;
