import { BasePaginationQueryDto } from '../dto/base-pagination-query.dto';

type RequiredPaginationKeys = Required<keyof BasePaginationQueryDto>;

export const getPaginatedRedisKey = (
  baseKey: string,
  { limit, skip }: BasePaginationQueryDto | Record<RequiredPaginationKeys, string | number>,
) => `${baseKey}-limit-${limit ?? 'NO'}-skip-${skip ?? 'NO'}`;
