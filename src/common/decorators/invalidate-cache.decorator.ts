import { SetMetadata, applyDecorators } from '@nestjs/common';
import { CACHE_INVALIDATE_KEY } from './invalidate-cache.const';

export const InvalidateCache = (cacheKey: string) =>
  applyDecorators(SetMetadata(CACHE_INVALIDATE_KEY, cacheKey));
