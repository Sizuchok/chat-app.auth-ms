import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { Types } from 'mongoose';

@Injectable()
export class ParseObjectIdPipe implements PipeTransform<string, string> {
  transform(value: string) {
    const isObjectId = Types.ObjectId.isValid(value);

    if (!isObjectId) {
      throw new BadRequestException('Validation failed (ObjectId string is expected)');
    }

    return value;
  }
}
