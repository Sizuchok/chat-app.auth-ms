import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';
import { AUTH_MS_CLIENT_ID, AUTH_MS_CONSUMER_ID } from './common/const/kafka/kafka-ms-config.const';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
    }),
  );

  const configService = app.get(ConfigService);
  const KAFKA_BROKER = configService.get('KAFKA_BROKER_URL') ?? '';

  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.KAFKA,
    options: {
      client: { brokers: [KAFKA_BROKER], clientId: AUTH_MS_CLIENT_ID },
      consumer: { groupId: AUTH_MS_CONSUMER_ID },
    },
  });

  await app.startAllMicroservices();
  await app.listen(3000);
}
bootstrap();
