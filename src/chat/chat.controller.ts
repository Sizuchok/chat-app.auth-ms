import { CacheInterceptor, CacheKey, CacheTTL } from '@nestjs/cache-manager';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Inject,
  OnModuleInit,
  Param,
  Patch,
  Post,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
import { CHAT_MS_KEY } from '../common/const/ms-keys.const';
import { ParamsWithIdDto } from '../common/dto/params-with-id.dto';
import { Public } from '../iam/decorators/public/public.decorator';
import { ChatService } from './chat.service';
import { CHAT } from './const/chat-events.const';
import { AddParticipantsDto } from './dto/add-participants-to-chat.dto';
import { CreateChatDto } from './dto/create-chat.dto';
import { UpdateChatDto } from './dto/update-chat.dto';
import { REDIS_KEY_ALL_CHATS } from './const/chat-redis-keys.const';
import { CacheInvalidationInterceptor } from '../common/interceptors/cache-invalidation.interceptor';
import { InvalidateCache } from '../common/decorators/invalidate-cache.decorator';
import { FindAllChatsQueryDto } from './dto/find-all-chats.dto';
import { PaginatedCacheInterceptor } from '../common/interceptors/paginated-cache.interceptor';

@Public() // TODO: remove
@UseInterceptors(CacheInvalidationInterceptor)
@Controller('chats')
export class ChatController implements OnModuleInit {
  constructor(
    private readonly chatService: ChatService,
    @Inject(CHAT_MS_KEY) private readonly chatClient: ClientKafka,
  ) {}

  @InvalidateCache(REDIS_KEY_ALL_CHATS)
  @Post()
  createChat(@Body() createChatDto: CreateChatDto) {
    return this.chatService.createChat(createChatDto);
  }

  @CacheTTL(24 * 3600 * 1000)
  @CacheKey(REDIS_KEY_ALL_CHATS)
  @UseInterceptors(PaginatedCacheInterceptor)
  @Get()
  findAllChats(@Query() findAllChatsQueryDto: FindAllChatsQueryDto) {
    return this.chatService.findAllChats(findAllChatsQueryDto);
  }

  @Get(':id')
  findChat(@Param() { id }: ParamsWithIdDto) {
    return this.chatService.findChat(id);
  }

  @InvalidateCache(REDIS_KEY_ALL_CHATS)
  @Patch(':id')
  updateChat(@Param() { id }: ParamsWithIdDto, @Body() updateChatDto: UpdateChatDto) {
    return this.chatService.updateChat(id, updateChatDto);
  }

  @InvalidateCache(REDIS_KEY_ALL_CHATS)
  @Patch(':id/add-participants')
  addParticipants(
    @Param() { id }: ParamsWithIdDto,
    @Body() addParticipantsDto: AddParticipantsDto,
  ) {
    return this.chatService.addParticipantsToChat(id, addParticipantsDto);
  }

  @InvalidateCache(REDIS_KEY_ALL_CHATS)
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async removeChat(@Param() { id }: ParamsWithIdDto) {
    await this.chatService.removeChat(id);
  }

  onModuleInit() {
    this.chatClient.subscribeToResponseOf(CHAT.GET_ALL);
    this.chatClient.subscribeToResponseOf(CHAT.CREATE);
    this.chatClient.subscribeToResponseOf(CHAT.GET_ONE);
    this.chatClient.subscribeToResponseOf(CHAT.ADD_PARTICIPANTS);
    this.chatClient.subscribeToResponseOf(CHAT.NEW_UNREAD_MESSAGES);
    this.chatClient.subscribeToResponseOf(CHAT.REMOVE_ONE);
    this.chatClient.subscribeToResponseOf(CHAT.UPDATE_ONE);
  }
}
