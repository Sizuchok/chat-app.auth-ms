import { Module } from '@nestjs/common';
import { ChatController } from './chat.controller';
import { ChatService } from './chat.service';
import { UsersModule } from '../users/users.module';

@Module({
  controllers: [ChatController],
  providers: [ChatService],
  imports: [UsersModule],
})
export class ChatModule {}
