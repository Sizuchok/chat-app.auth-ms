export enum ChatRole {
  Owner = 'owner',
  Admin = 'admin',
  Member = 'member',
}
