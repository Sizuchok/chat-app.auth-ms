export const CHAT = {
  CREATE: 'chat.create',
  GET_ALL: 'chat.get-all',
  GET_ONE: 'chat.get-one',
  UPDATE_ONE: 'chat.update-one',
  ADD_PARTICIPANTS: 'chat.add-participants',
  REMOVE_ONE: 'chat.remove-one',
  NEW_UNREAD_MESSAGES: 'chat.new-unread-messages',
} as const;
