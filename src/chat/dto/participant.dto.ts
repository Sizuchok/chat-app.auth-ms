import { User } from '../../users/entities/user.entity';
import { ChatRole } from '../const/chat-role.enum';

export class ParticipantDto {
  userId: string;
  name: string;
  nickname: string;
  email: string;
  surname?: string;
  role?: ChatRole;

  constructor(chatUser: User, role?: ChatRole) {
    this.userId = chatUser.id;
    this.name = chatUser.name;
    this.nickname = chatUser.nickname;
    this.surname = chatUser.surname;
    this.role = role;
    this.email = chatUser.email;
  }
}
