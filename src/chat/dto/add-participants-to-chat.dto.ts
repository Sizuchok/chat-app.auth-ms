import { ArrayMinSize, IsArray, IsMongoId } from 'class-validator';

export class AddParticipantsDto {
  @ArrayMinSize(1, { message: 'At least one participant should be specified.' })
  @IsArray()
  @IsMongoId({ each: true })
  participants: string[];
}
