import { ArrayMinSize, IsArray, IsMongoId, IsNotEmpty, IsString } from 'class-validator';

export class CreateChatDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @ArrayMinSize(2, { message: 'A chat must have at least two participants.' })
  @IsArray()
  @IsMongoId({ each: true })
  participants: string[];

  @IsMongoId()
  ownerId: string;
}
