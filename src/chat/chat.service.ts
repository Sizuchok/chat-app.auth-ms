import { BadRequestException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import { CHAT_MS_KEY } from '../common/const/ms-keys.const';
import { MongooseUser } from '../users/types/user.types';
import { UsersService } from '../users/users.service';
import { CHAT } from './const/chat-events.const';
import { ChatRole } from './const/chat-role.enum';
import { AddParticipantsDto } from './dto/add-participants-to-chat.dto';
import { CreateChatDto } from './dto/create-chat.dto';
import { ParticipantDto } from './dto/participant.dto';
import { UpdateChatDto } from './dto/update-chat.dto';
import { FindAllChatsQueryDto } from './dto/find-all-chats.dto';

@Injectable()
export class ChatService {
  constructor(
    @Inject(CHAT_MS_KEY) private readonly chatClient: ClientKafka,
    private readonly usersService: UsersService,
  ) {}

  async createChat(createChatDto: CreateChatDto) {
    const users = await this.usersService.findManyUsersByIds(createChatDto.participants);

    if (users.length < 2) {
      throw new BadRequestException(
        'A chat must have at least two participants. Check that the ids are valid and not duplicated.',
      );
    }

    const participants = users.map((user) => {
      return new ParticipantDto(user, this.getOwnerRole(createChatDto.ownerId, user));
    });

    return this.chatClient.send(CHAT.CREATE, { participants, name: createChatDto.name });
  }

  private getOwnerRole(ownerId: string, user: MongooseUser): ChatRole | undefined {
    return ownerId === user.id ? ChatRole.Owner : undefined;
  }

  findAllChats(findAllChatsQueryDto: FindAllChatsQueryDto) {
    return this.chatClient.send(CHAT.GET_ALL, { ...findAllChatsQueryDto });
  }

  async findChat(id: string) {
    const chat = await lastValueFrom(this.chatClient.send(CHAT.GET_ONE, id));

    if (!chat) {
      throw new NotFoundException('Chat not found');
    }

    return chat;
  }

  updateChat(id: string, updateChatDto: UpdateChatDto) {
    return this.chatClient.send(CHAT.UPDATE_ONE, { id, data: updateChatDto });
  }

  async addParticipantsToChat(id: string, addParticipantsDto: AddParticipantsDto) {
    const users = await this.usersService.findManyUsersByIds(addParticipantsDto.participants);

    if (users.length < 1) {
      throw new BadRequestException(
        'At least one participant should be specified. Check that the ids are valid and not duplicated.',
      );
    }

    const participants = users.map((user) => new ParticipantDto(user));

    return this.chatClient.send(CHAT.ADD_PARTICIPANTS, { id, participants });
  }

  async removeChat(id: string) {
    await lastValueFrom(this.chatClient.send(CHAT.REMOVE_ONE, id), { defaultValue: 'DELETED' });
  }
}
