import { UpdateQuery } from 'mongoose';
import { BcryptService } from '../../iam/bcrypt/bcrypt.service';
import { UserSchema } from './user.entity';
import { UpdateUserDto } from '../dto/update-user.dto';

export const hashPasswordOnSaveHook = (bcryptService: BcryptService) =>
  UserSchema.pre('save', async function (next) {
    if (!this.isNew) {
      return next();
    }

    this.password = await bcryptService.hash(this.password);
    next();
  });

export const hashPasswordOnUpdateHook = (bcryptService: BcryptService) =>
  UserSchema.pre('findOneAndUpdate', async function (next) {
    const updateQuery: UpdateQuery<UpdateUserDto> | null = this.getUpdate();
    const payload = updateQuery?.$set;

    if (!payload?.password) {
      return next();
    }

    payload.password = await bcryptService.hash(payload.password);
    next();
  });
