import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as _ from 'lodash';

@Schema({ timestamps: true })
export class User extends Document {
  @Prop({ required: true })
  name: string;

  @Prop()
  surname?: string;

  @Prop({ unique: true, required: true, set: (value) => _.snakeCase(value) })
  nickname: string;

  @Prop({ unique: true, required: true, lowercase: true })
  email: string;

  @Prop({ required: true, select: false })
  password: string;

  @Prop({ default: false, required: true, select: false })
  isVerified: boolean;
}

export const UserSchema = SchemaFactory.createForClass(User);
