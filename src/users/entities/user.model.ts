import { AsyncModelFactory } from '@nestjs/mongoose';
import { BcryptModule } from '../../iam/bcrypt/bcrypt.module';
import { BcryptService } from '../../iam/bcrypt/bcrypt.service';
import { User, UserSchema } from './user.entity';
import { hashPasswordOnSaveHook, hashPasswordOnUpdateHook } from './user.hooks';

export const UserModel: AsyncModelFactory = {
  name: User.name,
  useFactory: (bcryptService: BcryptService) => {
    hashPasswordOnSaveHook(bcryptService);
    hashPasswordOnUpdateHook(bcryptService);
    return UserSchema;
  },
  imports: [BcryptModule],
  inject: [BcryptService],
};
