import { Document, Types } from 'mongoose';
import { User } from '../entities/user.entity';

export type MongooseUser = Document<unknown, unknown, User> &
  User & {
    _id: Types.ObjectId;
  };
