import { MongooseUser } from '../types/user.types';

export class UserWithoutPasswordDto {
  id: string;
  name: string;
  surname?: string;
  nickname: string;
  email: string;
  isVerified: boolean;

  constructor(user: MongooseUser) {
    this.id = user.id;
    this.name = user.name;
    this.surname = user.surname;
    this.nickname = user.nickname;
    this.email = user.email;
    this.isVerified = user.isVerified;
  }
}
