import { IsEmail, IsOptional, IsString, IsStrongPassword } from 'class-validator';

export class CreateUserDto {
  @IsString()
  name: string;

  @IsOptional()
  surname: string;

  @IsString()
  nickname: string;

  @IsEmail()
  email: string;

  @IsStrongPassword()
  password: string;
}
