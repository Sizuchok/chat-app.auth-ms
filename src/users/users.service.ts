import {
  BadRequestException,
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { SignUpRequestDto } from '../iam/authentication/dto/sign-up-request.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { MongooseUser } from './types/user.types';
import { UserWithoutPasswordDto } from './dto/user-without-password.dto';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<User>) {}

  async createUser(signUpRequestDto: SignUpRequestDto): Promise<UserWithoutPasswordDto> {
    const user = await this.userModel.create(signUpRequestDto).catch((err) => {
      if (err.code === 11000) {
        const isNickname = (err.message as string).includes('nickname');

        const message = `A user with this ${
          isNickname ? 'nickname' : 'email'
        } is already registered`;

        throw new ConflictException(message);
      }

      throw err;
    });

    return new UserWithoutPasswordDto(user);
  }

  async findAll() {
    return this.userModel.find().exec();
  }

  async findOne(id: string) {
    const user = await this.userModel.findById(id, { password: false }).exec();

    if (!user) {
      throw new NotFoundException('User not found');
    }

    return user;
  }

  async findUserByIdDontThrow(id: string): Promise<MongooseUser | null> {
    return this.userModel.findById(id).exec();
  }

  async findUserByUniqueId(uniqueId: string): Promise<MongooseUser | null> {
    return this.userModel
      .findOne({
        $or: [{ email: uniqueId }, { nickname: uniqueId }],
      })
      .select('+password +isVerified')
      .exec();
  }

  async findManyUsersByIds(ids: string[]) {
    const objectIds = ids.map((id) => new Types.ObjectId(id));

    return this.userModel.find({ _id: { $in: objectIds } }).exec();
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    const updatedUser = await this.userModel
      .findOneAndUpdate({ _id: id }, { $set: updateUserDto }, { new: true })
      .exec();

    if (!updatedUser) {
      throw new BadRequestException('User not found for update');
    }

    return updatedUser;
  }

  async remove(id: string) {
    const res = await this.userModel.deleteOne({ _id: id });

    if (!res.deletedCount) {
      throw new BadRequestException('User was not found for deletion');
    }
  }
}
