import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ChatModule } from './chat/chat.module';
import { MsClientsModule } from './common/modules/ms-clients.module';
import { mongodbConfig } from './configs/db/mongodb.config';
import { IamModule } from './iam/iam.module';
import { UsersModule } from './users/users.module';
import { CacheModule } from '@nestjs/cache-manager';
import { redisStore } from 'cache-manager-ioredis-yet';
import { RedisOptions } from 'ioredis';

@Module({
  imports: [
    // TODO: work on env vars service
    ConfigModule.forRoot({ isGlobal: true }),
    IamModule,
    UsersModule,
    MongooseModule.forRootAsync(mongodbConfig.asProvider()),
    MsClientsModule,
    ChatModule,
    CacheModule.registerAsync<RedisOptions>({
      useFactory: (configService: ConfigService) => {
        return {
          store: redisStore,
          host: configService.get('REDIS_HOST'),
          port: +configService.get('REDIS_PORT'),
          ttl: +configService.get('REDIS_TOKENS_TTL'),
        };
      },
      inject: [ConfigService],
      isGlobal: true,
    }),
  ],
})
export class AppModule {}
