FROM node as builder

WORKDIR /app

COPY / .

RUN yarn install --frozen-lockfile

COPY . .

CMD ["yarn", "start:dev"]